@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cтворення команди</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.panel-body -->

    <!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Панель для внесення даних
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form role="form">
                            <div class="form-group">
                                <label>Закріпити лідера</label>
                                <select class="form-control">
                                    <option>Введіть дані</option>
                                    <option>Іванов В.В.</option>
                                    <option>Данилюк А.А.</option>
                                    <option>Степась І.І</option>
                                    <option>Данелишин В.А.</option>
                                </select>
                            </div>
                            <label>Дата завершення</label>
                            <div class="container">
                                <div class="col-sm-6" style="height:130px;">
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker11'>
                                            <input type='text' class="form-control" />
                                            <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                              </div>
                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-default">Зберегти</button>
                            <button type="reset" class="btn btn-default">Зберегти як чорновик</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /.panel-footer -->
</div>
<!-- /.panel .chat-panel -->
</div>
<!-- /.col-lg-4 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

@endsection