@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Запит на створення КБВ (А3)</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Дата ініціації </th>
                            <th>Ініціатор</th>
                            <th>Показник</th>
                            <th>Відхилення</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="danger">
                            <td><a href="/adits">3321</a></td>
                            <td>24/08/2017</td>
                            <td>Іванов І.І.</td>
                            <td>Споживання гaзу</td>
                            <td>1,5 тис.</td>
                        </tr>
                        <tr>
                        <tr class="danger">
                            <td><a href="/adits">3320</a></td>
                            <td>23/08/2017</td>
                            <td>Іванов І.І.</td>
                            <td>Вакуум</td>
                            <td>5,5 %</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.col-lg-4 (nested) -->
            <div class="col-lg-8">
                <div id="morris-bar-chart"></div>
            </div>
            <!-- /.col-lg-8 (nested) -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.panel-body -->

    <!-- /.row -->
</div>
<!-- /.panel-footer -->
</div>
<!-- /.panel .chat-panel -->
</div>
<!-- /.col-lg-4 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

@endsection