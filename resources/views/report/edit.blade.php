@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Панель для внесення даних
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form">
                                <div class="form-group">
                                    <label>Планова величина</label>
                                    <input class="form-control" placeholder="Введіть показник">
                                </div>
                                <div class="form-group">
                                    <label>Фактична величина</label>
                                    <input class="form-control" placeholder="Введіть показник">
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label>Прикріпити звіт</label>-->
<!--                                    <input type="file">-->
<!--                                </div>-->
<!--                                <div class="form-group">-->
<!--                                    <label>Коментар по виконанню показника</label>-->
<!--                                    <textarea class="form-control" rows="3"></textarea>-->
<!--                                </div>-->
<!--                                <div class="form-group">-->
<!--                                    <label>Досягнуто?</label>-->
<!--                                    <label class="checkbox-inline">-->
<!--                                        <input type="checkbox">Так-->
<!--                                    </label>-->
<!--                                    <label class="checkbox-inline">-->
<!--                                        <input type="checkbox">Ні-->
<!--                                    </label>-->
<!--                                </div>-->
                                <div class="form-group">
                                    <label>Оберіть процес</label>
                                    <select class="form-control">
                                        <option>Приймання палива (Жд.колії, розморожуючі пристрої)</option>
                                        <option>Зберігання палива (Вугільні склади)</option>
                                        <option>Пилоприготування (СПП)</option>
                                        <option>Виробництво пари (Котли)</option>
                                        <option>Створення крутного моменту (Турбіни)</option>
                                        <option>Генерація (Генератори)</option>
                                        <option>Трансформація (Трансформатори)</option>
                                        <option>Розподіл елктроенергії (ВРП)</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Оберіть показник</label>
                                    <select class="form-control">
                                        <option>Споживання газу</option>
                                        <option>Власні потреби</option>
                                        <option>ПВУП</option>
                                        <option>Вакуум</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Оберіть № блоку</label>
                                    <select multiple class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                    </select>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label>Оберіть підрозділи які впливають на даний показник</label>-->
<!--                                    <select multiple class="form-control">-->
<!--                                        <option>КТЦ</option>-->
<!--                                        <option>ЦЦР</option>-->
<!--                                        <option>ЕЦ</option>-->
<!--                                        <option>ЦТАВ</option>-->
<!--                                        <option>ХЦ</option>-->
<!--                                    </select>-->
<!--                                </div>-->
                                <button type="submit" class="btn btn-default">Зберегти</button>
                                <button type="reset" class="btn btn-default">Зберегти як чорновик</button>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->


@endsection