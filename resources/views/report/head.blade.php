@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ $title }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-print fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>Cтворено стандартів операцій!</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Переглянути</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-file-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>Не актуальних стандартів (СОП) </div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Переглянути</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-tasks fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>Cтворено нових завдань!</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Переглянути</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-bell fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>Протермінованих завдань</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Переглянути</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class=" fa fa-crosshairs fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>Відцифрованих показників КПЕ</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Переглянути</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-bell fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>КПЕ які не <br>виконуються</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Переглянути</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-th fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>КПСЦ нинішнього <br> стану</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Переглянути</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-th-large fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>КПСЦ майбутнього стану</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Переглянути</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bullseye   fa-fw"></i>Досягнення матриці зрілості
                </div>
                <!-- /.panel-heading -->
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Показник</th>
                        <th>Ціль</th>
                        <th>Факт</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Кількість відхилень</td>
                        <td>250 шт.</td>
                        <td>0 шт.</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Час реагування на відхилення</td>
                        <td>4 дні</td>
                        <td>0 дні</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Кількість проблем</td>
                        <td>134 шт.</td>
                        <td>0 шт.</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Доля вирішених проблем</td>
                        <td>20 %</td>
                        <td>0 %</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Кількість СОП</td>
                        <td>78 шт.</td>
                        <td>0 шт.</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Актуальність СОП</td>
                        <td>80 %</td>
                        <td>0 %</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Кількість КПЕ</td>
                        <td>26 шт.</td>
                        <td>0 шт.</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Кількість роцедур</td>
                        <td>100 шт.</td>
                        <td>0 шт.</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Доля виконаних завдань</td>
                        <td>40 %</td>
                        <td>0 %</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Кількість КПСЦ</td>
                        <td>17 шт.</td>
                        <td>0 шт.</td>
                    </tr>
                    </tbody>
                </table>
                <div class="panel-body">
                    <div style="display: none" id="morris-area-chart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-5">
            <div class="panel panel-default">
                    <div class="chat-panel panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-stumbleupon fa-fw"></i> Карта потоку створення ціностей
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="height: 482px">
                            <ul class="chat">
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="image/1.png" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font">Приймання палива</strong>
                                        </div>
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </li>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="image/2.png" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font">Зберігання палива</strong>
                                        </div>
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </li>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="image/3.png" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font">Пилоприготування</strong>
                                        </div>
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </li>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="image/4.png" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font">Виробництво пари</strong>
                                        </div>
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </li>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="image/5.png" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font">Створення крутного моменту</strong>
                                        </div>
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </li>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="image/6.png" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font">Генерація</strong>
                                        </div>
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </li>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="image/7.png" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font">Трансформація</strong>
                                        </div>
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </li>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="image/8.png" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font">Розподіл електроенергії</strong>
                                        </div>
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>

        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-edit   fa-fw"></i> Актуальність СОП
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th style="font-size: 10px;">КПЕ які мають прямий вплив на EBITDA</th>
                                <th style="font-size: 10px;color: #2ff71b;">Загальна кількість</th>
                                <th style="font-size: 10px;color: #f7543d;">Не дійсних</th>
                                <th style="font-size: 10px;color: #f7b143;">Не актуальних</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="odd gradeX">
                                <td style="font-size: 9px;">Тривалість еквівалентного змушеного простою EFOR</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;"class="center">0</td>
                            </tr>
                            <tr class="even gradeC">
                                <td style="font-size: 9px;">Надійність роботи обладнання</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;"class="center">0</td>
                            </tr>
                            <tr class="odd gradeA">
                                <td style="font-size: 9px;">Виконання планового показника УРУТ на корисний відпуск електроенергії</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;"class="center">0</td>
                            </tr>
                            <tr class="even gradeA">
                                <td style="font-size: 9px;">Виконання планового показника по витраті газу на пуски блоків із різних теплових станів</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;"class="center">0</td>
                            </tr>
                            <tr class="odd gradeA">
                                <td style="font-size: 9px;">Не перевищення норм витрати електроенергії</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;"class="center">0</td>
                            </tr>
                            <tr class="even gradeA">
                                <td style="font-size: 9px;">Не перевищення питомої витрати дизельного палива на одну мото/годину</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;"class="center">0</td>
                            </tr>
                            <tr class="even gradeA">
                                <td style="font-size: 9px;">Перепали палива</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;"class="center">0</td>
                            </tr>
                            <tr class="gradeA">
                                <td style="font-size: 9px;">Не перевищення планової витрати ПММ</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;"class="center">0</td>
                            </tr>
                            <tr class="gradeA">
                                <td style="font-size: 9px;">Дотримання середньогодинної нормативної велечини підживлення в теплових мережах міста</td>
                                <td style="font-size: 15px;color: #2ff71b;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7543d;"class="center">0</td>
                                <td style="font-size: 15px;color: #f7b143;" class="center">0</td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa  fa-bell-o fa-fw"></i> Кількість втрат за класифікацією
                </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="#" class="list-group-item">
                                <i class="fa fa-database fa-fw"></i> Втрати через перевиробництво
                                <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-eye fa-fw"></i> Втрати часу через очікування
                                <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-wrench fa-fw"></i> Втрати через зайві етапи обробки
                                <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-truck fa-fw"></i> Втрати при непотрібному транспортуванні
                                <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-cubes fa-fw"></i> Втрати через зайві запаси
                                <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-road fa-fw"></i> Втрати через непотрібні переміщення
                                <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-warning fa-fw"></i> Втрати через випуск дефектної продукції
                                <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-male fa-fw"></i> Втрати через людський фактор
                                <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                            </a>
                        </div>
                        <!-- /.list-group -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa  fa-th-list fa-fw"></i> Дошка проблем
                </div>
                <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-donut-chart"></div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
    </div>
</div>
@endsection
