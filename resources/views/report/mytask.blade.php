@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.col-lg-12 -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <!-- /.panel-heading -->
                <div class="panel-body" style="height: 750px">
                    <div class="row">
                        <div class="col-lg-12">
                            <p>
                                <button type="button" class="btn btn-default"><i class="fa fa-wrench fa-fw"></i>Редагувати</button>
                           &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                <button type="button" class="btn btn-primary"><i class="fa fa-search-plus fa-fw"></i>Виріщується</button>
                                <button type="button" class="btn btn-success"><i class="fa fa-check-circle fa-fw"></i>Вирішені</button>
                                <button type="button" class="btn btn-danger"><i class="fa fa-question-circle fa-fw"></i>Не вирішені</button>
                            </p>
                        </div>
                        <div class="col-lg-12">
                            <div class="title" style="color: #2a2a2a; font-size: 150%">Деталі завдання</div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <h5>Проблема:</h5>
                                            <h5>Завдання:</h5>
                                            <h5>Статус:</h5>
                                            <h5>Ініціатор:</h5>
                                            <h5>Підрозділ:</h5>
                                        </div>
                                        <div class="col-lg-7">
                                            <h5><a href="/kns">Не якісне обстеження обладнання</a> </h5>
                                            <h5>Провести дефектацію</h5>
                                            <h5><p class="" style="border-radius: 5%; background-color: #20c7e2;float: left; width: 100%;color: rgb(242,245,254); padding-left: 20px;padding-right: 10px;font-weight: 400;font-size: 100%" >Вирішені</p></h5>
                                            <h5>Іваніцький В.В. <img src="image/ava5.png" style="height: 35px; float: right; position: inherit;"></h5>
                                            <h5>КТЦ</h5>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-lg-4">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h5>Вкладені файли:</h5>
                                            <h5>Дата назначення:</h5>
                                            <h5>Дата виконання:</h5>
                                            <h5>Відповідальний:</h5>
                                            <h5>Процент виконання:</h5>
                                            <h5>Тип завдання:</h5>
                                        </div>
                                        <div class="col-lg-6">
                                            <h5>Відсутні</h5>
                                            <h5>30.08.2017</h5>
                                            <h5>06.10.2017</h5>
                                            <h5>Кропельник С.В.</h5>
                                            <h5>100 %</h5>
                                            <h5><p class="" style="border-radius: 5%; background-color: #f7e01d;float: right; color: #05130e; padding-left: 10px; font-weight: 600;padding-right: 10px;font-size: 80%" >Інвестиції</p></h5>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="title" style="color: #2a2a2a; font-size: 150%">Вкладені файли</div>
                            <div class="panel-body">
                                <h4 style="text-align:center; font-family: 'Times New Roman', Times, serif; /* Шрифт с засечками */
    font-style: italic; /* Курсивное начертание */border-radius: 1%; border: 1px solid #d3d7d0;padding: 30px;"><img src="image/slaid.png" style="height:20%; padding-right: 40px; border-radius: 10px; border-color: #2a2a2a;b">Перенесіть файл, щоб закріпити</h4>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-12">
                            <div class="title" style="color: #2a2a2a; font-size: 150%">Коментар по виконанню</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label>Коментар</label>
                                    <textarea class="form-control" rows="3" placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."></textarea>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
    @endsection
    <!-- /.row -->