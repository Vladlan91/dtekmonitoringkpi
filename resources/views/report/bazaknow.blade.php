@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.col-lg-12 -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel-body" style="height: 150px">
                            <div class="flot-chart">
                                <div class="form-group">
                                    <label>Оберіть за процесом</label>
                                    <select class="form-control">
                                        <option>Приймання палива (Жд.колії, розморожуючі пристрої)</option>
                                        <option>Зберігання палива (Вугільні склади)</option>
                                        <option>Пилоприготування (СПП)</option>
                                        <option>Виробництво пари (Котли)</option>
                                        <option>Створення крутного моменту (Турбіни)</option>
                                        <option>Генерація (Генератори)</option>
                                        <option>Трансформація (Трансформатори)</option>
                                        <option>Розподіл елктроенергії (ВРП)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel-body" style="height: 150px">
                            <div class="flot-chart">
                                <div class="form-group">
                                    <label>Оберіть за КПЕ які мають прямий вплив на EBITDA</label>
                                    <select class="form-control">
                                        <option>Тривалість еквівалентного змушеного простою EFOR</option>
                                        <option>Надійність роботи обладнання</option>
                                        <option>Виконання планового показника УРУТ на корисний відпуск електроенергії</option>
                                        <option>Виконання планового показника по витраті газу на пуски блоків із різних теплових станів</option>
                                        <option>Не перевищення норм витрати електроенергії</option>
                                        <option>Не перевищення питомої витрати дизельного палива на одну мото/годину</option>
                                        <option>Перепали палива</option>
                                        <option>Не перевищення планової витрати ПММ</option>
                                        <option>Дотримання середньогодинної нормативної велечини підживлення в теплових мережах міста</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="well">
                        <p>DataTables is a very flexible, advanced tables plugin for jQuery. In SB Admin, we are using a specialized version of DataTables built for Bootstrap 3. We have also customized the table headings to use Font Awesome icons in place of images. For complete documentation on DataTables, visit their website at <a target="_blank" href="https://datatables.net/">https://datatables.net/</a>.</p>
                        <a class="btn btn-default btn-lg btn-block" target="_blank" href="#">Пошук</a>
                    </div>
            </div>
            <!-- /.panel -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    DataTables Advanced Tables
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Назва</th>
                            <th>Ініціатор</th>
                            <th>Лідер команди</th>
                            <th>Дата початку</th>
                            <th>Дата завершення</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="odd gradeX">
                            <td><a href="/kns">Не якісне обстеження обладнання</a></td>
                            <td>Iваніцький В.В.</td>
                            <td>Кропельник С.В.</td>
                            <td>30.08.2017</td>
                            <td>06.10.2017</td>
                        </tr>
                        <tr class="even gradeC">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="odd gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="even gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="odd gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="even gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr class="gradeA">
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection
<!-- /.row -->