@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Дата ініціації </th>
                            <th>Ініціатор</th>
                            <th>Показник</th>
                            <th>Відхилення</th>
                            <th>Статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <tr>
                            <td><a href="/more">3320</a></td>
                            <td>23/08/2017</td>
                            <td>Іванов І.І.</td>
                            <td>Вакуум</td>
                            <td>5,5 %</td>
                            <td><p class="" style="border-radius: 5%; background-color: #20c7e2;float: left; width: 100%;color: rgb(242,245,254); padding-left: 20px;padding-right: 10px;font-weight: 400;font-size: 80%" >Аналізується</p></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.col-lg-4 (nested) -->
            <div class="col-lg-8">
                <div id="morris-bar-chart"></div>
            </div>
            <!-- /.col-lg-8 (nested) -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.panel-body -->

</div>
@endsection