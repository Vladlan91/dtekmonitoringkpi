@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$title}}</h1>
        </div>

        <!-- /.col-lg-12 -->

    <!-- /.row -->
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3><i class="fa fa-sitemap fa-fw"></i>Бізнес - логіки системи</h3>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><i class="fa fa-sitemap fa-fw"></i>EBITDA Створення показників верхнього рівня</h4>
                            <button type="button" class="btn btn-info btn-circle btn-lg"><i class="fa fa-check"></i></button>
                        </div>
                        <!-- /.panel-heading -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><i class="fa fa-crosshairs fa-fw"></i>КПЕ Створення показників нижнього рівня</h4>
                            <button type="button" class="btn btn-success btn-circle btn-lg"><i class="fa fa-link"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><i class="fa fa-user-md fa-fw"></i>Створення процедур</h4>
                            <button type="button" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-list"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><i class="fa fa-gears fa-fw"></i>Створення структури бізнес - процесів</h4>
                            <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-check"></i></button>
                        </div>
                    </div>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
@endsection