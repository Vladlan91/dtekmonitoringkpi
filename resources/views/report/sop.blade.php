@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-code-fork fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>Кількість <br>процедур</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">Переглянути</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-print fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>Cтворено стандартів операцій!</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">Переглянути</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-file-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>Не актуальних <br> СОП </div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">Переглянути</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-th fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>Не дійсних <br> СОП</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">Переглянути</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><i class="fa fa-edit  fa-fw"></i>Актуальність СОП</h3>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>КПЕ які мають прямий вплив на EBITDA</th>
                            <th style="color: #2ff71b;" >Загальна кількість СОП</th>
                            <th style="color: #f7543d;" >Не дійсних</th>
                            <th style="color: #f7b143;" >Не актуальних</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="odd gradeX">
                            <td>Тривалість еквівалентного змушеного простою EFOR</td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        <tr class="even gradeC">
                            <td>Надійність роботи обладнання</td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        <tr class="odd gradeA">
                            <td><a href="report-show">Виконання планового показника УРУТ на корисний відпуск електроенергії</a></td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        <tr class="even gradeA">
                            <td>Виконання планового показника по витраті газу на пуски блоків із різних теплових станів</td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        <tr class="odd gradeA">
                            <td>Не перевищення норм витрати електроенергії</td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        <tr class="even gradeA">
                            <td>Не перевищення питомої витрати дизельного палива на одну мото/годину</td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        <tr class="even gradeA">
                            <td>Перепали палива</td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        <tr class="gradeA">
                            <td>Не перевищення планової витрати ПММ</td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        <tr class="gradeA">
                            <td>Дотримання середньогодинної нормативної велечини підживлення в теплових мережах міста</td>
                            <td class="center" style="color: #2ff71b;">0</td>
                            <td class="center" style="color: #f7543d;">0</td>
                            <td class="center" style="color: #f7b143;">0</td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3><h3><i class="fa fa-gears fa-fw"></i>Кількість СОП з привязкою до бізнес процесів</h3>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th style="font-size: 9px;">Приймання палива</th>
                                    <th style="font-size: 9px;">Зберігання палива</th>
                                    <th style="font-size: 9px;">Пилоприготування</th>
                                    <th style="font-size: 9px;">Виробництво пари</th>
                                    <th style="font-size: 9px;">Створення крутного моменту</th>
                                    <th style="font-size: 9px;">Генерація</th>
                                    <th style="font-size: 9px;">Трансформація</th>
                                    <th style="font-size: 9px;">Розподіл електроенергії</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="odd gradeX">
                                    <td class="center">0</td>
                                    <td class="center">0</td>
                                    <td class="center">0</td>
                                    <td class="center">0</td>
                                    <td class="center">0</td>
                                    <td class="center">0</td>
                                    <td class="center">0</td>
                                    <td class="center">0</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3><i class="fa fa-user-md fa-fw"></i>Кількість СОП  по видам діяльності</h3>
                        </div>
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>Вид діяльності</th>
                                <th style="color: #2ff71b;" >Загальна кількість СОП</th>
                                <th style="color: #f7543d;" >Не дійсних</th>
                                <th style="color: #f7b143;" >Не актуальних</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="odd gradeX">
                                <td>Планування та аналітика</td>
                                <td class="center" style="color: #2ff71b;">0</td>
                                <td class="center" style="color: #f7543d;">0</td>
                                <td class="center" style="color: #f7b143;">0</td>
                            </tr>
                            <tr class="odd gradeX">
                                <td>Вхідний контроль</td>
                                <td class="center" style="color: #2ff71b;">0</td>
                                <td class="center" style="color: #f7543d;">0</td>
                                <td class="center" style="color: #f7b143;">0</td>
                            </tr>
                            <tr class="even gradeC">
                                <td>Ремонтна діяльність</td>
                                <td class="center" style="color: #2ff71b;">0</td>
                                <td class="center" style="color: #f7543d;">0</td>
                                <td class="center" style="color: #f7b143;">0</td>
                            </tr>
                            <tr class="odd gradeA">
                                <td>Експлуатаційна діяльність</a></td>
                                <td class="center" style="color: #2ff71b;">0</td>
                                <td class="center" style="color: #f7543d;">0</td>
                                <td class="center" style="color: #f7b143;">0</td>
                            </tr>
                            <tr class="even gradeA">
                                <td>Технічне обслуговування</td>
                                <td class="center" style="color: #2ff71b;">0</td>
                                <td class="center" style="color: #f7543d;">0</td>
                                <td class="center" style="color: #f7b143;">0</td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->


                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                @endsection