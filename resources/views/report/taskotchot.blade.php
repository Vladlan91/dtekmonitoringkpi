@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.col-lg-12 -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="flot-chart">
                        <div class="row">
                            <div class="col-lg-6">
                               Не виришені завдання: По приорітету
                                <div class="panel-body">
                                    <h4>Приорітет</h4>
                                    <div class="list-group">
                                        <a href="#" class="list-group-item">
                                            <img src="image/high.png"> High
                                            <span class="pull-right text-muted"><em>4</em>
                                    </span>
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <img src="image/medium.png"> Medium
                                            <span class="pull-right text-muted"><em>3</em>
                                    </span>
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <img src="image/low.png"> Low
                                            <span class="pull-right text-muted"><em>8</em>
                                    </span>
                                        </a>
                                    </div>
                                    <!-- /.list-group -->
                                </div>
                                <hr>
                            </div>
                            <div class="col-lg-6">
                                Завдання по статусу
                                <div class="row">
                                <div class="col-lg-6">
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <h4>Cтатус</h4>
                                        <div class="list-group">
                                            <a href="#" class="list-group-item">Вирішуються<span class="pull-right text-muted "><em>12</em></span></a>
                                            <a href="#" class="list-group-item">Вирішені<span class="pull-right text-muted"><em>3</em></span></a>
                                            <a href="#" class="list-group-item">Не вирішені<span class="pull-right text-muted "><em>15</em></span></a>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-lg-6">
                                        <div class="panel-body">
                                                <h4>Процент</h4>
                                                <div style="background-color: #0083ee; height: 22px;width: 180px; border-radius: 3%; margin-bottom: 20px;margin-top: 20px"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 37%</h5></div>
                                                <div style="background-color: #0083ee; height: 22px;width: 80px; border-radius: 3%; margin-bottom: 20px;margin-top: 20px"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 7%</h5></div>
                                                <div style="background-color: #0083ee; height: 22px;width: 200px; border-radius: 3%; margin-bottom: 20px;margin-top: 20px"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 46%</h5></div>

                                            </div>
                                            <!-- /.list-group -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>       <!-- /.list-group -->
                                </div>
                                <hr>
                            </div>
                            <div class="col-lg-12">
                                Не вирішених: По впливу на EBITDA
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <h4>Назва</h4>
                                            <p class="text-primary">Тривалість еквівалентного змушеного простою EFOR</p>
                                            <p class="text-primary">Надійність роботи обладнання</p>
                                            <p class="text-primary">Виконання планового показника УРУТ на корисний відпуск електроенергії</p>
                                            <p class="text-primary">Виконання планового показника по витраті газу на пуски блоків із різних теплових станів</p>
                                            <p class="text-primary">Не перевищення норм витрати електроенергії</p>
                                            <p class="text-primary">Не перевищення питомої витрати дизельного палива на одну мото/годину</p>
                                            <p class="text-primary">Перепали палива</p>
                                            <p class="text-primary">Не перевищення планової витрати ПММ</p>
                                            <p class="text-primary">Дотримання середньогодинної нормативної велечини підживлення в теплових мережах міста</p>

                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="panel-body">
                                            <h4>Завдань</h4>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="panel-body">
                                            <h4>Проблем</h4>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="col-lg-6">
                                Не вирішених: По відповідальному
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <h4>Виконавець</h4>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <img src="image/ava.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div>
                                                <div class="col-lg-3">
                                                    <img src="image/ava1.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava2.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava3.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava4.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava5.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava6.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava7.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="panel-body">
                                            <h4>Завдань</h4>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="panel-body">
                                            <h4>Процент</h4>
                                            <div style="background-color: #0083ee; height: 22px;width: 115px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 20%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 105px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 18%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 40px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 5%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 40px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 5%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 55px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 8%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 40px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 2%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 75px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 12%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 115px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 20%</h5></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>

                            <div class="col-lg-6">
                                Не вирішених: По відповідальному
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <h4>Виконавець</h4>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <img src="image/ava.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div>
                                                <div class="col-lg-3">
                                                    <img src="image/ava1.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava2.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava3.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava4.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava5.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava6.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div> <div class="col-lg-3">
                                                    <img src="image/ava7.png" style="height: 30px; padding-bottom: 10px;">
                                                </div>
                                                <div class="col-lg-9">
                                                    <p class="text-primary">Іваніцький В.В.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="panel-body">
                                            <h4>Проблем</h4>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="panel-body">
                                            <h4>Процент</h4>
                                            <div style="background-color: #0083ee; height: 22px;width: 115px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 20%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 105px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 18%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 40px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 5%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 40px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 5%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 55px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 8%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 40px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 2%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 75px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 12%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 115px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 20%</h5></div>

                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="col-lg-6">
                                Не вирішених: По типу
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <h4>Тип завдання</h4>
                                            <p class="text-primary">Інвестиції</p>
                                            <p class="text-primary">Господарчий спосіб</p>
                                            <p class="text-primary">Беззатратні</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="panel-body">
                                            <h4>Завдань</h4>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                            <p class="text-primary">1</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="panel-body">
                                            <h4>Процент</h4>
                                            <div style="background-color: #0083ee; height: 22px;width: 100px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 30%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 130px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 40%</h5></div>
                                            <div style="background-color: #0083ee; height: 22px;width: 100px; border-radius: 3%"><h5 style="color:#fffbff; padding-left: 3px;padding-top: 3px"> 30%</h5></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- /.row -->