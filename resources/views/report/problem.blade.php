@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.col-lg-12 -->
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class=" fa fa-th-list fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>Загальна кількість проблем</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer" style="background-color: rgb(5,19,14)">
                        <span class="pull-left">Переглянути</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa  fa-spinner fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>Кількість проблем на стадії вирішення</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer" style="background-color: rgb(5,19,14)">
                        <span class="pull-left">Переглянути</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-check fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>Кількість вирішених проблем</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer" style="background-color: rgb(5,19,14)">
                        <span class="pull-left">Переглянути</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel" style="background-color: rgba(13,54,37,0.19)">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-ban fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>Кількість проблем які не взято в розробку</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer" style="background-color: rgb(5,19,14)">
                        <span class="pull-left">Переглянути</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Pill Tabs
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="nav nav-pills">
                    <li class="active"><a href="#home-pills" data-toggle="tab">Всі проблеми</a>
                    </li>
                    <li><a href="#profile-pills" data-toggle="tab">Вирішуються</a>
                    </li>
                    <li><a href="#messages-pills" data-toggle="tab">Вирішені</a>
                    </li>
                    <li><a href="#no-pills" data-toggle="tab">Не вирішені</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home-pills">
                        <h4>Home Tab</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Показник</th>
                                                <th>Підрозділ</th>
                                                <th>Процедура</th>
                                                <th>Величина відхилення</th>
                                                <th>Дата виявлення</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="danger">
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                            </tr>
                                            <tr class="danger">
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                            </tr>
                                            <tr class="danger">
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                            </tr>
                                            <tr class="danger">
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                                <td>--//--</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    </div>
                    <div class="tab-pane fade" id="profile-pills">
                        <h4>Profile Tab</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Показник</th>
                                                    <th>Підрозділ</th>
                                                    <th>Процедура</th>
                                                    <th>Величина відхилення</th>
                                                    <th>Дата виявлення</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="warning">
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                </tr>
                                                <tr class="warning">
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                </tr>
                                                <tr class="warning">
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                </tr>
                                                <tr class="warning">
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                            </div>
                            <!-- /.col-lg-6 -->
                        </div>
                    </div>
                    <div class="tab-pane fade" id="messages-pills">
                        <h4>Messages Tab</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Показник</th>
                                                    <th>Підрозділ</th>
                                                    <th>Процедура</th>
                                                    <th>Величина відхилення</th>
                                                    <th>Дата виявлення</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="info">
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                </tr>
                                                <tr class="info">
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                </tr>
                                                <tr class="info">
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                </tr>
                                                <tr class="info">
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                    <td>--//--</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                            </div>
                            <!-- /.col-lg-6 -->
                        </div>
                    </div>
                        <div class="tab-pane fade" id="no-pills">
                            <h4>Homnjjjj</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Показник</th>
                                                        <th>Підрозділ</th>
                                                        <th>Процедура</th>
                                                        <th>Величина відхилення</th>
                                                        <th>Дата виявлення</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr style="background-color: rgba(13,54,37,0.19)">
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                    </tr>
                                                    <tr style="background-color: rgba(13,54,37,0.19)">
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                    </tr>
                                                    <tr style="background-color: rgba(13,54,37,0.19)">
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                    </tr>
                                                    <tr style="background-color: rgba(13,54,37,0.19)">
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                        <td>--//--</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>
                                <!-- /.col-lg-6 -->
                            </div>
                        </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
</div>
@endsection
