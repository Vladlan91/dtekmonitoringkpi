@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
                <div id="page-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Показники діяльності ПП які мають прямий вплив на EBITDA</h1>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class=" fa fa-male fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">0</div>
                                            <div>Кількість <br>КПЕ</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">Переглянути</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-adjust fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">0</div>
                                            <div>Кількість<br>відхилень</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">Переглянути</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-code-fork fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">0</div>
                                            <div>Кількість <br>процедур</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">Переглянути</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-puzzle-piece fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">0</div>
                                            <div>Кількість <br>СОП</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">Переглянути</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                                    <div class="panel-body">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Тривалість еквівалентного змушеного простою EFOR</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                    <div class="panel-body">

                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                            <div class="panel-body">
                                                                <div class="table-responsive table-bordered">
                                                                    <table class="table">
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="font-size: 9px;">Кількість КПЕ</th>
                                                                            <th style="font-size: 9px;">Кількість відхилень</th>
                                                                            <th style="font-size: 9px;">Кількість процедур</th>
                                                                            <th style="font-size: 9px;">Кількість СОП</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>0</td>
                                                                            <td>0</td>
                                                                            <td>0</td>
                                                                            <td>0</td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!-- /.table-responsive -->
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive table-bordered">
                                                                        <table class="table">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="font-size: 9px;">Планування</th>
                                                                                <th style="font-size: 9px;">Уксплуатація</th>
                                                                                <th style="font-size: 9px;">Технічне обслуговування</th>
                                                                                <th style="font-size: 9px;">Ремонт</th>

                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- /.table-responsive -->
                                                                </div>
                                                            </div>
                                                            <!-- /.panel -->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4>Показники впливу</h4>
                                                                    </div>
                                                                    <!-- /.panel-heading -->
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>Назва показника</th>
                                                                                    <th>Процес</th>
                                                                                    <th>Підрозділ впливу</th>
                                                                                    <th>Вид</th>
                                                                                    <th>Процедура</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="well">
                                                                            <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">Створити новий показник</a>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                                <!-- /.panel -->
                                                            </div>

                                                        </div>
                                                            <!-- /.col-lg-6 -->
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Надійність роботи обладнання</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive table-bordered">
                                                                        <table class="table">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="font-size: 9px;">Кількість КПЕ</th>
                                                                                <th style="font-size: 9px;">Кількість відхилень</th>
                                                                                <th style="font-size: 9px;">Кількість процедур</th>
                                                                                <th style="font-size: 9px;">Кількість СОП</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- /.table-responsive -->
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive table-bordered">
                                                                        <table class="table">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="font-size: 9px;">Планування</th>
                                                                                <th style="font-size: 9px;">Уксплуатація</th>
                                                                                <th style="font-size: 9px;">Технічне обслуговування</th>
                                                                                <th style="font-size: 9px;">Ремонт</th>

                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- /.table-responsive -->
                                                                </div>
                                                            </div>
                                                            <!-- /.panel -->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4>Показники впливу</h4>
                                                                    </div>
                                                                    <!-- /.panel-heading -->
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>Назва показника</th>
                                                                                    <th>Процес</th>
                                                                                    <th>Підрозділ впливу</th>
                                                                                    <th>Вид</th>
                                                                                    <th>Процедура</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="well">
                                                                            <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">Створити новий показник</a>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                                <!-- /.panel -->
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Виконання планового показника УРУТ на корисний відпуск електроенергії</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive table-bordered">
                                                                        <table class="table">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="font-size: 9px;">Кількість КПЕ</th>
                                                                                <th style="font-size: 9px;">Кількість відхилень</th>
                                                                                <th style="font-size: 9px;">Кількість процедур</th>
                                                                                <th style="font-size: 9px;">Кількість СОП</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- /.table-responsive -->
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive table-bordered">
                                                                        <table class="table">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="font-size: 9px;">Планування</th>
                                                                                <th style="font-size: 9px;">Уксплуатація</th>
                                                                                <th style="font-size: 9px;">Технічне обслуговування</th>
                                                                                <th style="font-size: 9px;">Ремонт</th>

                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- /.table-responsive -->
                                                                </div>
                                                            </div>
                                                            <!-- /.panel -->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4>Показники впливу</h4>
                                                                    </div>
                                                                    <!-- /.panel-heading -->
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>Назва показника</th>
                                                                                    <th>Процес</th>
                                                                                    <th>Підрозділ впливу</th>
                                                                                    <th>Вид</th>
                                                                                    <th>Процедура</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                    <td>--//--</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="well">
                                                                            <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">Створити новий показник</a>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                                <!-- /.panel -->
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFore">Виконання планового показника по витраті газу на пуски блоків із різних теплових станів</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseFore" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Планування</th>
                                                                                    <th style="font-size: 9px;">Уксплуатація</th>
                                                                                    <th style="font-size: 9px;">Технічне обслуговування</th>
                                                                                    <th style="font-size: 9px;">Ремонт</th>

                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <!-- /.panel -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4>Показники впливу</h4>
                                                                        </div>
                                                                        <!-- /.panel-heading -->
                                                                        <div class="panel-body">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-striped">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Назва показника</th>
                                                                                        <th>Процес</th>
                                                                                        <th>Підрозділ впливу</th>
                                                                                        <th>Вид</th>
                                                                                        <th>Процедура</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="well">
                                                                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">Створити новий показник</a>
                                                                            </div>
                                                                            <!-- /.table-responsive -->
                                                                        </div>
                                                                        <!-- /.panel-body -->
                                                                    </div>
                                                                    <!-- /.panel -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Не перевищення норм витрати електроенергії</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseFive" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Планування</th>
                                                                                    <th style="font-size: 9px;">Уксплуатація</th>
                                                                                    <th style="font-size: 9px;">Технічне обслуговування</th>
                                                                                    <th style="font-size: 9px;">Ремонт</th>

                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <!-- /.panel -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4>Показники впливу</h4>
                                                                        </div>
                                                                        <!-- /.panel-heading -->
                                                                        <div class="panel-body">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-striped">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Назва показника</th>
                                                                                        <th>Процес</th>
                                                                                        <th>Підрозділ впливу</th>
                                                                                        <th>Вид</th>
                                                                                        <th>Процедура</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="well">
                                                                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">Створити новий показник</a>
                                                                            </div>
                                                                            <!-- /.table-responsive -->
                                                                        </div>
                                                                        <!-- /.panel-body -->
                                                                    </div>
                                                                    <!-- /.panel -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Не перевищення питомої витрати дизельного палива на одну мото/годину</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseSix" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Планування</th>
                                                                                    <th style="font-size: 9px;">Уксплуатація</th>
                                                                                    <th style="font-size: 9px;">Технічне обслуговування</th>
                                                                                    <th style="font-size: 9px;">Ремонт</th>

                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <!-- /.panel -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4>Показники впливу</h4>
                                                                        </div>
                                                                        <!-- /.panel-heading -->
                                                                        <div class="panel-body">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-striped">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Назва показника</th>
                                                                                        <th>Процес</th>
                                                                                        <th>Підрозділ впливу</th>
                                                                                        <th>Вид</th>
                                                                                        <th>Процедура</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="well">
                                                                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">Створити новий показник</a>
                                                                            </div>
                                                                            <!-- /.table-responsive -->
                                                                        </div>
                                                                        <!-- /.panel-body -->
                                                                    </div>
                                                                    <!-- /.panel -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Перепали палива</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseSeven" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Планування</th>
                                                                                    <th style="font-size: 9px;">Уксплуатація</th>
                                                                                    <th style="font-size: 9px;">Технічне обслуговування</th>
                                                                                    <th style="font-size: 9px;">Ремонт</th>

                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <!-- /.panel -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4>Показники впливу</h4>
                                                                        </div>
                                                                        <!-- /.panel-heading -->
                                                                        <div class="panel-body">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-striped">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Назва показника</th>
                                                                                        <th>Процес</th>
                                                                                        <th>Підрозділ впливу</th>
                                                                                        <th>Вид</th>
                                                                                        <th>Процедура</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="well">
                                                                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">Створити новий показник</a>
                                                                            </div>
                                                                            <!-- /.table-responsive -->
                                                                        </div>
                                                                        <!-- /.panel-body -->
                                                                    </div>
                                                                    <!-- /.panel -->
                                                                </div>

                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            <!-- .panel-body -->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEit">Не перевищення планової витрати ПММ</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseEit" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Кількість КПЕ</th>
                                                                                    <th style="font-size: 9px;">Кількість відхилень</th>
                                                                                    <th style="font-size: 9px;">Кількість процедур</th>
                                                                                    <th style="font-size: 9px;">Кількість СОП</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive table-bordered">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th style="font-size: 9px;">Планування</th>
                                                                                    <th style="font-size: 9px;">Уксплуатація</th>
                                                                                    <th style="font-size: 9px;">Технічне обслуговування</th>
                                                                                    <th style="font-size: 9px;">Ремонт</th>

                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                    <td>0</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- /.table-responsive -->
                                                                    </div>
                                                                </div>
                                                                <!-- /.panel -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4>Показники впливу</h4>
                                                                        </div>
                                                                        <!-- /.panel-heading -->
                                                                        <div class="panel-body">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-striped">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Назва показника</th>
                                                                                        <th>Процес</th>
                                                                                        <th>Підрозділ впливу</th>
                                                                                        <th>Вид</th>
                                                                                        <th>Процедура</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                        <td>--//--</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="well">
                                                                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">Створити новий показник</a>
                                                                            </div>
                                                                            <!-- /.table-responsive -->
                                                                        </div>
                                                                        <!-- /.panel-body -->
                                                                    </div>
                                                                    <!-- /.panel -->
                                                                </div>

                                                            </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- .panel-body -->
                                </div>
                                <!-- /.panel -->
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                </div>
                <!-- /.panel-body -->
@endsection
<!-- /.row -->