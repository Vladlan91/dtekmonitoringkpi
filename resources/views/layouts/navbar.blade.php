@section('navbar')
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="/head"><i class="fa fa-bar-chart fa-fw"></i>Статистика</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-users fa-fw"></i>Підрозділи<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/homeptc">ПТЦ</a>
                    </li>
                    <li>
                        <a href="/homeec">ЕЦ</a>
                    </li>
                    <li>
                        <a href="/homektc">КТЦ</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="/report"><i class="fa fa-mail-reply fa-fw"></i> Відхилення</a>
            </li>
            <li>
                <a href="/problem"><i class="fa fa-th-list fa-fw"></i> Дошка проблем</a>
            </li>
            <li>
                <a href="/task"><i class="fa fa-list-alt fa-fw"></i> Диспетчер завдань</a>
            </li>
            <li>
                <a href="/sops"><i class="fa fa-print fa-fw"></i> СОП</a>
            </li>
            <li>
                <a href="/kpis"><i class="fa fa-crosshairs fa-fw"></i>КПЕ</a>
            </li>
            <li>
                <a href="/baza"><i class="fa fa-joomla   fa-fw"></i> База знань</a>
            </li>
            <li>
                <a href="/pokaz"><i class="fa fa-sitemap fa-fw"></i>EBITDA</a>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="/proces"><i class="fa fa-gears fa-fw"></i>Бізнес - процеси</a>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> Внести <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">СОП</a>
                    </li>
                    <li>
                        <a href="/show">Коментар по відхиленню</a>
                    </li>
                    <li>
                        <a href="/edit">Статистику</a>
                    </li>
                    <li>
                        <a href="/show">Звіт по проекту А3</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="/nalasht"><i class="fa fa-wrench fa-fw"></i>Налаштування</a>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
@endsection