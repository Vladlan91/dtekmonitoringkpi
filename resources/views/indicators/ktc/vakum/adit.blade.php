@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Вакуум</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Дата</th>
                            <th>План</th>
                            <th>Факт</th>
                            <th>Відхилення</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>3321</td>
                            <td>25/08/2017</td>
                            <td>91 %</td>
                            <td>92,5 %</td>
                            <td>1,5 %</td>
                        </tr>
                        <tr>
                            <td>3321</td>
                            <td>24/08/2017</td>
                            <td>91 %</td>
                            <td>92,5 %</td>
                            <td>1,5 %</td>
                        </tr>
                        <tr>
                        <tr class="danger">
                                <td>3320</td>
                                <td>23/08/2017</td>
                                <td>90 %</td>
                                <td>85,5 %</td>
                                <td>5,5 %</td>
                        </tr>
                        <tr>
                            <td>3319</td>
                            <td>22/08/2017</td>
                            <td>91 %</td>
                            <td>91,5 %</td>
                            <td>0,5 %</td>
                            </td>
                        </tr>
                        <tr>
                            <td>3321</td>
                            <td>21/08/2017</td>
                            <td>91 %</td>
                            <td>92,5 %</td>
                            <td>1,5 %</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.col-lg-4 (nested) -->
            <div class="col-lg-8">
                <div id="morris-bar-chart"></div>
            </div>
            <!-- /.col-lg-8 (nested) -->
        </div>
        <!-- /.row -->
    </div>
                <!-- /.panel-body -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Причини відхилення
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form">
                                <div class="form-group">
                                    <label>Прикріпити документ</label>
                                    <input type="file">
                                </div>
                                <div class="form-group">
                                    <label>Коментар по виконанню показника</label>
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Ініціювати А3?</label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox">Так
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox">Ні
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-default">Зберегти</button>
                                <button type="reset" class="btn btn-default">Зберегти як чорновик</button>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
                <!-- /.panel-footer -->
            </div>
            <!-- /.panel .chat-panel -->
        </div>
        <!-- /.col-lg-4 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

@endsection