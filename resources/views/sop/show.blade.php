@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $title }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- /.row -->
        <div class="col-lg-4">
            <div class="panel panel-green">
                <div class="panel-heading">
                    Актуальний
                </div>
                <div class="panel-body">
                    <p>Послідовність дій у споживані <br>газу.</p>
                </div>
                <div class="panel-footer">
                    <a href="/sop_vakum">Переглянути</a>
                </div>
            </div>
            <!-- /.col-lg-4 -->
        </div>

        <div class="col-lg-4">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    Не актуальний
                </div>
                <div class="panel-body">
                    <p>Використання термоочистки кондесаторів ТГ.</p>
                </div>
                <div class="panel-footer">
                    <a href="/sop_vakum">Переглянути</a>
                </div>
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <div class="col-lg-4">
            <div class="panel panel-red">
                <div class="panel-heading">
                    Потребує перегляду
                </div>
                <div class="panel-body">
                    <p>Пуск енергоблоку з холодного <br>стану.</p>
                </div>
                <div class="panel-footer">
                    <a href="/sop_vakum">Переглянути</a>
                </div>
            </div>
            <!-- /.col-lg-4 -->
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection