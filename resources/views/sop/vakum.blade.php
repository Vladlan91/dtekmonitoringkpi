@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Вакyум</h1>
        </div>
           <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Дата</th>
                                <th>План</th>
                                <th>Факт</th>
                                <th>Відхилення</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            <tr class="success">
                                    <td>3320</td>
                                    <td>19/08/2017</td>
                                    <td>90 %</td>
                                    <td>93,5 %</td>
                                    <td>3,5 %</td>
                            </tr>
                            </tr>
                            </tbody>
                        </table>
                        <div class="panel-heading">
                            Цільовий показник вакууму
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon">%</span>
                            <input type="text" class="form-control" value="92">
                            <span class="input-group-addon">.00</span>
                        </div>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.col-lg-4 (nested) -->
                <div class="col-lg-8">
                    <div id="morris-bar-chart"></div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                               Використання термоочистки кондесаторів ТГ
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body" style="height: 1560px">
                                <div class="flot-chart">
                                    <img src="image/one.png" style="width: 100%">
                                    <img src="image/two.png" style="width: 100%">
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
@endsection