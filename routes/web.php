<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/homeptc', 'HomeController@indexptc');
Route::get('/homeec', 'HomeController@indexec');
Route::get('/homektc', 'HomeController@indexktc');
Route::get('/sop', 'SopController@index');
Route::get('/sop-ktc', 'SopController@show_some');
Route::get('/report', 'ReportController@index');
Route::get('/edit', 'ReportController@edit');
Route::get('/head', 'ReportController@head');
Route::get('/kpis', 'ReportController@kpis');
Route::get('/kns', 'ReportController@kns');
Route::get('/mytask', 'ReportController@mytask');
Route::get('/taskotchot', 'ReportController@taskotchot');
Route::get('/baza', 'ReportController@baza');
Route::get('/sops', 'ReportController@sops');
Route::get('/task', 'ReportController@task');
Route::get('/nalasht', 'ReportController@nalasht');
Route::get('/problem', 'ReportController@problem');
Route::get('/more', 'ReportController@more');
Route::get('/proces', 'ReportController@proces');
Route::get('/pokaz', 'ReportController@pokaz');
Route::get('/gas', 'IndicatorKtcController@index');
Route::get('/vakum', 'IndicatorKtcController@vakum');
Route::get('/adits', 'IndicatorKtcController@edits');
Route::get('/adit_v', 'IndicatorKtcController@adit_v');
Route::get('/show', 'IndicatorKtcController@shows');
Route::get ('/sop_vakum','PhotoController@index');
Route::get('/report-show', 'ReportController@show');



