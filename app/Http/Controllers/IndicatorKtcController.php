<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndicatorKtcController extends Controller
{
    public function index()
    {
        $title = "Підрозділ КТЦ";
        return view('indicators.ktc.gas.show')->withTitle($title);
    }

    public function vakum()
    {
        $title = "Підрозділ КТЦ";
        return view('indicators.ktc.vakum.show')->withTitle($title);
    }

    public function adit_v()
    {
        $title = "Підрозділ КТЦ";
        return view('indicators.ktc.vakum.adit')->withTitle($title);
    }

    public function edits()
    {
        $title = "Підрозділ КТЦ";
        return view('a3.edit')->withTitle($title);
    }
    public function shows()
    {
        $title = "Підрозділ КТЦ";
        return view('a3.show')->withTitle($title);
    }
}
