<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Область звітностей ДТЕК БУРШТИНСЬКА ТЕС";
        return view('home')->withTitle($title);
    }
    public function indexptc()
    {
        $title = "Область звітностей ПТЦ";
        return view('home')->withTitle($title);
    }
    public function indexec()
    {
        $title = "Область звітностей ЕЦ";
        return view('home')->withTitle($title);
    }
    public function indexktc()
    {
        $title = "Область звітностей КТЦ";
        return view('home')->withTitle($title);
    }
}
