<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        $title = "Звіти та статистика по ТЕС";
        return view('report.show')->withTitle($title);
    }

    public function edit()
    {
        $title = "Область звітностей";
        return view('report.edit')->withTitle($title);
    }

    public function show()
    {
        $title = "Область відхилень";
        return view('report.vakum')->withTitle($title);
    }
    public function more()
    {
        $title = "Вакуум";
        return view('report.detal')->withTitle($title);
    }
    public function pokaz()
    {
        $title = "Вакуум";
        return view('report.pokazniki')->withTitle($title);
    }
    public function proces()
    {
        $title = "Бізнес - процеси ";
        return view('report.proces')->withTitle($title);
    }
    public function problem()
    {
        $title = "Електрона дошка проблем ";
        return view('report.problem')->withTitle($title);
    }
    public function kpis()
    {
        $title = "Ключеві показники ефективності роботи ДТЕК БУРШТИНСЬКА ТЕС ";
        return view('report.kpi')->withTitle($title);
    }
    public function sops()
    {
        $title = "Стандарти операційної процедури ДТЕК БУРШТИНСЬКА ТЕС ";
        return view('report.sop')->withTitle($title);
    }
    public function nalasht()
    {
        $title = "Область налаштувань";
        return view('report.nalasht')->withTitle($title);
    }
    public function head()
    {
        $title = "Статистика ДТЕК БУРШТИНСЬКА ТЕС";
        return view('report.head')->withTitle($title);
    }
    public function task()
    {
        $title = "Диспетчер завдань";
        return view('report.task')->withTitle($title);
    }
    public function baza()
    {
        $title = "Архів розроблених заходів";
        return view('report.bazaknow')->withTitle($title);
    }
    public function kns()
    {
        $title = "Не якісне обстеження обладнання";
        return view('report.kns')->withTitle($title);
    }
    public function taskotchot()
    {
        $title = "Звіт по роботі персоналу";
        return view('report.taskotchot')->withTitle($title);
    }
    public function mytask()
    {
        $title = "Звіт по виконанню завдання";
        return view('report.mytask')->withTitle($title);
    }
}
