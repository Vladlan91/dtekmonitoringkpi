<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SopController extends Controller
{
    public function index()
    {
        $title = "Стандарти БуТЕС";
        return view('sop.show')->withTitle($title);
    }
    public function show_some()
    {
        $title = "Стандарти по підрозділу КТЦ";
        return view('sop.show')->withTitle($title);
    }
}
